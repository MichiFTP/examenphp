<?php

class DeedRepository {
    const TABLE_NAME = "PIFDeeds";
    const COL_ID = "IDDeed";
    const COL_TITLE = "Title";
    const COL_USER = "User";
    const COL_DESCRIPTION = "Description";
    const COL_TAG = "Tag";
    const COL_SCORE = "Score";
    const COL_DATE = "Date";
    
    public function find($id) {
        global $dbConn;
        $sql = "SELECT ".self::COL_ID.", ".self::COL_TITLE.", ".self::COL_USER. ", ".self::COL_DESCRIPTION.", ".self::COL_TAG.", ".self::COL_SCORE.", ".self::COL_DATE." FROM ".self::TABLE_NAME." WHERE IDDeed =  ".$id;
        $result = $dbConn->query($sql);
        
        if ($row=$result->fetch_assoc()) {
            $deed = new Deed();
            $deed->setId($row[self::COL_ID]);
            $deed->setTitle($row[self::COL_TITLE]);
            $deed->setUser($row[self::COL_USER]);
            $deed->setDescription($row[self::COL_DESCRIPTION]);
            $deed->setTags($row[self::COL_TAG]);
            $deed->setScore($row[self::COL_SCORE]);
            $deed->setDate($row[self::COL_DATE]);
            return $deed;
        }
        return null;
    }
    
    public function findTotalLikes($id) {
        global $dbConn;
        $sql = "SELECT SUM(".self::COL_SCORE.") AS TotalScore FROM ".self::TABLE_NAME." WHERE User = ".$id;
        
        $result = $dbConn->query($sql);
        
        if ($row=$result->fetch_assoc()) {
            $deed = new Deed();
            $deed->setScore($row["TotalScore"]);
            return $deed;
        }
        return null;
    }
    
    public function findTotalDeeds($id) {
        global $dbConn;
        $sql = "SELECT COUNT(".self::COL_ID.") AS TotalDeeds FROM ".self::TABLE_NAME." WHERE User = ".$id;
        
        $result = $dbConn->query($sql);
        
        if ($row=$result->fetch_assoc()) {
            $deed = new Deed();
            $deed->setId($row["TotalDeeds"]);
            return $deed;
        }
        return null;
    }
    
    public function findBestDeedOverall() {
        global $dbConn;
        $sql = "SELECT IDDeed, Title, Description, Score FROM PIFDeeds ORDER BY Score DESC LIMIT 1";
        $result = $dbConn->query($sql);
        
        if ($row=$result->fetch_assoc()) {
            $deed = new Deed();
            $deed->setId($row[self::COL_ID]);
            $deed->setTitle($row[self::COL_TITLE]);
            $deed->setDescription($row[self::COL_DESCRIPTION]);
            $deed->setScore($row[self::COL_SCORE]);
            return $deed;
        }
        return null;
    }
    
    public function findBestDeedDay($date) {
        global $dbConn;
        $sql = "SELECT IDDeed, Title, Description, Score FROM PIFDeeds WHERE Date = '$date' ORDER BY Score DESC LIMIT 1";
        $result = $dbConn->query($sql);
        
        if ($row=$result->fetch_assoc()) {
            $deed = new Deed();
            $deed->setId($row[self::COL_ID]);
            $deed->setTitle($row[self::COL_TITLE]);
            $deed->setDescription($row[self::COL_DESCRIPTION]);
            $deed->setScore($row[self::COL_SCORE]);
            return $deed;
        }
        return null;
    }
    
    public function deleteDeed($id) {
        global $dbConn;
        $sql = "DELETE FROM PIFDeeds WHERE IDDeed = $id";
        $result = $dbConn->query($sql);
    }
    
    public function insertDeed($title, $idUser, $description, $tags, $date) {
        global $dbConn;
        $sql = "INSERT INTO PIFDeeds (Title, User, Description, Tag, Date) VALUES ('$title',$idUser,'$description','$tags','$date')";
        $result = $dbConn->query($sql);
    }
}
