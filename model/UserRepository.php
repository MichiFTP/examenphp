<?php

class UserRepository {
    const TABLE_NAME = "PIFUsers";
    const COL_ID = "IDUser";
    const COL_NAME = "Name";
    const COL_LASTNAME = "Lastname";
    const COL_USERNAME = "Username";
    const COL_EMAIL = "Email";
    const COL_PASSWORD = "Password";
    const COL_DATE = "DateRegistered";
    
    public function find($id) {
        global $dbConn;
        $sql = "SELECT ".self::COL_ID.", ".self::COL_NAME.", ".self::COL_LASTNAME. ", ".self::COL_USERNAME.", ".self::COL_EMAIL.", ".self::COL_PASSWORD.", ".self::COL_DATE." FROM ".self::TABLE_NAME." WHERE IDUser =  ".$id;
        $result = $dbConn->query($sql);
        
        if ($row=$result->fetch_assoc()) {
            $user = new User();
            $user->setId($row[self::COL_ID]);
            $user->setName($row[self::COL_NAME]);
            $user->setLastname($row[self::COL_LASTNAME]);
            $user->setUsername($row[self::COL_USERNAME]);
            $user->setEmail($row[self::COL_EMAIL]);
            $user->setPassword($row[self::COL_PASSWORD]);
            $user->setDateRegistered($row[self::COL_DATE]);
            return $user;
        }
        return null;
    }
    
    public function findName($naam) {
        global $dbConn;
        $sql = "SELECT ".self::COL_ID.", ".self::COL_NAME.", ".self::COL_LASTNAME. ", ".self::COL_USERNAME.", ".self::COL_EMAIL.", ".self::COL_PASSWORD.", ".self::COL_DATE." FROM ".self::TABLE_NAME." WHERE Name =  '".$naam."'";
        
        $result = $dbConn->query($sql);
        
        if ($row=$result->fetch_assoc()) {
            $user = new User();
            $user->setId($row[self::COL_ID]);
            $user->setName($row[self::COL_NAME]);
            $user->setLastname($row[self::COL_LASTNAME]);
            $user->setUsername($row[self::COL_USERNAME]);
            $user->setEmail($row[self::COL_EMAIL]);
            $user->setPassword($row[self::COL_PASSWORD]);
            $user->setDateRegistered($row[self::COL_DATE]);
            return $user;
        }
        return null;
    }
    
    public function findUsername($username) {
        global $dbConn;
        $sql = "SELECT ".self::COL_ID.", ".self::COL_NAME.", ".self::COL_LASTNAME. ", ".self::COL_USERNAME.", ".self::COL_EMAIL.", ".self::COL_PASSWORD.", ".self::COL_DATE." FROM ".self::TABLE_NAME." WHERE Username =  '".$username."'";
        
        $result = $dbConn->query($sql);
        
        if ($row=$result->fetch_assoc()) {
            $user = new User();
            $user->setId($row[self::COL_ID]);
            $user->setName($row[self::COL_NAME]);
            $user->setLastname($row[self::COL_LASTNAME]);
            $user->setUsername($row[self::COL_USERNAME]);
            $user->setEmail($row[self::COL_EMAIL]);
            $user->setPassword($row[self::COL_PASSWORD]);
            $user->setDateRegistered($row[self::COL_DATE]);
            return $user;
        }
        return null;
    }
}
