<?php

$name = $_SESSION["name"];
$Lname = $_SESSION["Lname"];
$date = date("Y-m-d");

$deedRep = new deedRepository();
$userRep = new UserRepository();
$user = $userRep->findName($name);
$idUser = $user->getID();

//SQL NAME
$sql = "SELECT Name, Lastname FROM PIFUsers";
$result = $dbConn->query($sql);
$row = $result->fetch_assoc();
$name = $row["Name"];
$lastname = $row["Lastname"];

if (isset($_POST["insert"])) {
     $title = $_POST["title"];
     $description = $_POST["description"];
     $tags = $_POST["tags"];
     
     $deed = $deedRep->insertDeed($title, $idUser, $description, $tags, $date);
     header("Location: ". $baseUrl. "home.php");
}

include("view/createDeed.html.php");
?>