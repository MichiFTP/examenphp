<?php

$userRep = new UserRepository();

if (isset($_POST["submit"])) {
    $username = $_POST["username"];
    $password = $_POST["password"];
    $hash = password_hash($password, PASSWORD_DEFAULT);
    
    $user = $userRep->findUsername($username);
    $name = $user->getName();
    $lastname = $user->getLastname();
    
    $_SESSION["username"] = $username;
    $_SESSION["name"] = $name;
    $_SESSION["Lname"] = $lastname;
    header("Location: ". $baseUrl. "home.php");
}

if (isset($_POST["signup"])) {
    header("Location: ". $baseUrl. "signup.php");
}

include("view/signin.html.php");
?>