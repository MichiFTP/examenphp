<?php

$date = date("Y-m-d");

$deedRep = new DeedRepository();

//SQL DEED OVERALL
$deed = $deedRep->findBestDeedOverall();
$idOverall = $deed->getId();
$titleOverall = $deed->getTitle();
$descriptionOverall = $deed->getDescription();
$scoreOverall = $deed->getScore();

//SQL DEED DAY
$deed = $deedRep->findBestDeedDay($date);
if ($deed == null) {
    $titleDay = "No deeds yet!";
    $descriptionDay = "Be the first to post one today!";
}else {
    $idDay = $deed->getId();
    $titleDay = $deed->getTitle();
    $descriptionDay = $deed->getDescription();
    $scoreDay= $deed->getScore();
}

//SQL DEEDS
$sql = "SELECT IDDeed, Title, Description, Score, Date FROM PIFDeeds ORDER BY Date DESC";
$result = $dbConn->query($sql);

if (isset($_POST["submit"])) {
  header("Location: ". $baseUrl. "createDeed.php");
}



//VOTE DAY
if (isset($_POST["upvoteDay"])) {
    $sql = "UPDATE PIFDeeds SET Score = Score + 1 WHERE IDDeed = $idDay";
    $result = $dbConn->query($sql);
    header("Location: ". $baseUrl. "home.php");
}
if (isset($_POST["downvoteDay"])) {
    $sql = "UPDATE PIFDeeds SET Score = Score - 1 WHERE IDDeed = $idDay";
    $result = $dbConn->query($sql);
    header("Location: ". $baseUrl. "home.php");
}
//VOTE OVERALL
if (isset($_POST["upvoteAll"])) {
    $sql = "UPDATE PIFDeeds SET Score = Score + 1 WHERE IDDeed = $idOverall";
    $result = $dbConn->query($sql);
    header("Location: ". $baseUrl. "home.php");
}
if (isset($_POST["downvoteAll"])) {
    $sql = "UPDATE PIFDeeds SET Score = Score - 1 WHERE IDDeed = $idOverall";
    $result = $dbConn->query($sql);
    header("Location: ". $baseUrl. "home.php");
}
//SELECT SINGLE DEED
if (isset($_POST["selectDeed"])) {
    $title = $_POST["selectDeed"];
    $_SESSION["deed"] = $title;
    header("Location: ". $baseUrl. "singleDeedGeneral.php");
}

include("view/home.html.php");
?>