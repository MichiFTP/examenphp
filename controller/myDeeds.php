<?php
$name = $_SESSION["name"];
$Lname = $_SESSION["Lname"];
$date = date("Y-m-d");

$userRep = new UserRepository();
$user = $userRep->findName($name);
$idUser = $user->getID();
$_SESSION["idUser"] = $idUser;

$sql = "SELECT IDDeed, Title, Description, Score, Date FROM PIFDeeds WHERE User = $idUser";
$result = $dbConn->query($sql);

if (isset($_POST["submit"])) {
  header("Location: ". $baseUrl. "createDeed.php");
}

if (isset($_POST["selectDeed"])) {
    $title = $_POST["selectDeed"];
    $_SESSION["deed"] = $title;
    header("Location: ". $baseUrl. "singleDeed.php");
}

include("view/myDeeds.html.php");
?>