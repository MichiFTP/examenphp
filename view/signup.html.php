<!doctype html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Pay it forward!</title>
      <!-- plugins:css -->
      <link rel="stylesheet" href="resources/vendors/mdi/css/materialdesignicons.min.css">
      <link rel="stylesheet" href="resources/vendors/base/vendor.bundle.base.css">
      <!-- endinject -->
      <!-- plugin css for this page -->
      <link rel="stylesheet" href="resources/vendors/datatables.net-bs4/dataTables.bootstrap4.css">
      <!-- End plugin css for this page -->
      <!-- inject:css -->
      <link rel="stylesheet" href="resources/css/style.css">
      <!-- endinject -->
      <link rel="shortcut icon" href="resources/images/favicon.png" />
      
      <meta name="description" content="Pay it forward">
      <meta name="author" content="MichiFTP">
    </head>
    
<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
                <h1>Deeds</h1>
              </div>
              <h4>New here?</h4>
              <h6 class="font-weight-light">Signing up is easy. It only takes a few steps</h6>
              <form class="pt-3" method="post" action="signup.php">
                <div class="form-group">
                  <a>Name:</a>
                  <input type="text" class="form-control form-control-lg" id="exampleInputName1" placeholder="Name" name="name">
                </div>
                <div class="form-group">
                  <a>Last name:</a>
                  <input type="text" class="form-control form-control-lg" id="exampleInputLastname1" placeholder="Lastname" name="Lname">
                </div>
                <div class="form-group">
                  <a>Username:</a>
                  <input type="text" class="form-control form-control-lg" id="exampleInputUsername1" placeholder="Username" name="user">
                </div>
                <div class="form-group">
                  <a>E-mail:</a>
                  <input type="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Email" name="email">
                </div>
                <div class="form-group">
                  <a>Password:</a>
                  <input type="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password" name="password">
                </div>
                <div class="mb-4">
                  <div class="form-check">
                    <label class="form-check-label text-muted">
                      <input type="checkbox" class="form-check-input">
                      I agree to all Terms &amp; Conditions
                    <i class="input-helper"></i></label>
                  </div>
                </div>
                <div class="mt-3">
                  <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" name="register">SIGN UP</button>
                </div>
                <div class="text-center mt-4 font-weight-light">
                  Already have an account? <a href="signin.php" class="text-primary">Login</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="../../vendors/base/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="../../js/off-canvas.js"></script>
  <script src="../../js/hoverable-collapse.js"></script>
  <script src="../../js/template.js"></script>
  <!-- endinject -->



</body>
</html>