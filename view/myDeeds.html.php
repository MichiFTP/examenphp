<!doctype html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Pay it forward!</title>
      <!-- plugins:css -->
      <link rel="stylesheet" href="resources/vendors/mdi/css/materialdesignicons.min.css">
      <link rel="stylesheet" href="resources/vendors/base/vendor.bundle.base.css">
      <!-- endinject -->
      <!-- plugin css for this page -->
      <link rel="stylesheet" href="resources/vendors/datatables.net-bs4/dataTables.bootstrap4.css">
      <!-- End plugin css for this page -->
      <!-- inject:css -->
      <link rel="stylesheet" href="resources/css/style.css">
      <!-- endinject -->
      <link rel="shortcut icon" href="resources/images/favicon.png" />
      
      <meta name="description" content="Pay it forward">
      <meta name="author" content="MichiFTP">
    </head>
    
    <body>
        <div class="container-scroller">
        <!-- partial:partials/_navbar.html -->
        <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
                <div class="navbar-brand-wrapper d-flex justify-content-center">
            <div class="navbar-brand-inner-wrapper d-flex justify-content-between align-items-center w-100">  
              <a class="navbar-brand brand-logo" href="home.php"><img src="../resources/images/Deeds.png" alt="logo"/></a>
              <a class="navbar-brand brand-logo-mini" href="home.php"><img src="../resources/images/Deeds.png" alt="logo"/></a>
              <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                <span class="mdi mdi-sort-variant"></span>
              </button>
            </div>  
          </div>
          <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
            <ul class="navbar-nav navbar-nav-right">
              <li class="nav-item nav-profile dropdown">
                <span class="nav-profile-name"><?= ($_SESSION["name"])?> <?= ($_SESSION["Lname"])?></span>
              </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
              <span class="mdi mdi-menu"></span>
            </button>
          </div>
        </nav>
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
          <!-- partial:partials/_sidebar.html -->
          <nav class="sidebar sidebar-offcanvas" id="sidebar">
            <ul class="nav">
              <li class="nav-item">
                <a class="nav-link" href="home.php">
                  <i class="mdi mdi-home menu-icon"></i>
                  <span class="menu-title">Home</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="">
                  <i class="mdi mdi-view-headline menu-icon"></i>
                  <span class="menu-title">My good deeds</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="myAccount.php">
                  <i class="mdi mdi-emoticon menu-icon"></i>
                  <span class="menu-title">My account</span>
                </a>
              </li>
            </ul>
          </nav>
          <!-- partial -->
          <div class="main-panel">
            <form class="forms-sample" method="post" action="myDeeds.php">
              <div class="content-wrapper">
                <div class="row">
                  <div class="col-md-12 grid-margin">
                    <div class="d-flex justify-content-between flex-wrap">
                      <div class="d-flex align-items-end flex-wrap">
                        <div class="mr-md-3 mr-xl-5">
                          <h2>View your deeds here!</h2>
                        </div>
                      </div>
                      <div class="d-flex justify-content-between align-items-end flex-wrap">
                        <button class="btn btn-primary mt-2 mt-xl-0" type="submit" name="submit">Create new deed</button>
                      </div>
                    </div>
                  </div>
                </div>
              <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <p class="card-title">Overview</p>
                          <?php while ($row = $result->fetch_assoc()):
                            $title = $row["Title"];
                            $date = $row["Date"];
                            $score= $row["Score"];
                            $id = $row["IDDeed"];
                          ?>
                          <button class="btn btn-info mt-2 mt-xl-0" type="submit" name="selectDeed" value="<?= ($title)?>"><?= ($title)?></button><p>Score: <?= ($score)?></p><p><?= ($date)?></p><br>
                          <?php endwhile;?>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <footer class="footer">
              <div class="d-sm-flex justify-content-center justify-content-sm-between">
                <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">MichiFTP</a>. All rights reserved.</span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
              </div>
            </footer>
            <!-- partial -->
          </div>
          <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
      </div>
      <!-- container-scroller -->
    
      <!-- plugins:js -->
      <script src="vendors/base/vendor.bundle.base.js"></script>
      <!-- endinject -->
      <!-- Plugin js for this page-->
      <script src="vendors/chart.js/Chart.min.js"></script>
      <script src="vendors/datatables.net/jquery.dataTables.js"></script>
      <script src="vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
      <!-- End plugin js for this page-->
      <!-- inject:js -->
      <script src="js/off-canvas.js"></script>
      <script src="js/hoverable-collapse.js"></script>
      <script src="js/template.js"></script>
      <!-- endinject -->
      <!-- Custom js for this page-->
      <script src="js/dashboard.js"></script>
      <script src="js/data-table.js"></script>
      <script src="js/jquery.dataTables.js"></script>
      <script src="js/dataTables.bootstrap4.js"></script>
      <!-- End custom js for this page-->
    </body>
</html>