<?php
$host = "83.217.67.11";
$username = "michiel";
$paswoord = "stoelen3";
$database = "michiel";

include('include/autoloader.php');

$dbConn =  mysqli_connect($host, $username, $paswoord, $database);

include('model/User.php');
include('model/UserRepository.php');
include('model/Deed.php');
include('model/DeedRepository.php');
session_start();

$uri = rtrim( dirname($_SERVER["SCRIPT_NAME"]), '/' );
$uri = '/' . trim( str_replace( $uri, '', $_SERVER['REQUEST_URI'] ), '/' );
$uri = urldecode( $uri );

$baseUrl = str_replace('index.php','',$_SERVER["SCRIPT_NAME"]);
//$baseUrl = $_SERVER["SCRIPT_NAME"];

$routes = array(
    'signin'            => "/",     
    'home'              => "/home.php",         
    'signup'            => "/signup.php",
    'createDeed'        => "/createDeed.php",
    'singleDeed'        => "/singleDeed.php",
    'myDeeds'           => "/myDeeds.php",
    'myAccount'         => "/myAccount.php",
    'singleDeedGeneral' => "/singleDeedGeneral.php",
);

foreach ($routes as $action => $route)
{
    if ( preg_match( '~^'.$route.'$~i', $uri, $routeParameters ) )
    {
        cleanup($routeParameters);
        include( 'controller/'.$action.'.php' );
        exit();
    }
}
include( 'view/404.html.php' );


function cleanup($array) {
    foreach ($array as $key=>$value){
        if(gettype($key)=='integer'){
            unset($array[$key]);
        }
    }
}